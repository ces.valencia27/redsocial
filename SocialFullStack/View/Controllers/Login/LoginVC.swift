//
//  LoginVC.swift
//  SocialFullStack
//
//  Created by Adri Liz Guti More on 10/5/19.
//  Copyright © 2019 Adri Liz Guti More. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    let UI: LoginView = {
        let UI = LoginView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        debugPrint("Ya cargué")
        setupUI()
    }
    
    // Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes de la vista principal del controlador
    private func setSubviews() {
        
        self.view.addSubview(UI)
    }
    
    // Método para definir el autolayout de la interfaz gráfica
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0),
            UI.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            UI.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            UI.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0)
        ])
    }

    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {
        debugPrint("Ahí les voy")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        debugPrint("Ya llegué")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        debugPrint("Ya me voy")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        debugPrint("Ya me fuí")
    }
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de manejor de memoria")
    }

}
