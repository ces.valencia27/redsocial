//
//  LoginView.swift
//  SocialFullStack
//
//  Created by Adri Liz Guti More on 10/5/19.
//  Copyright © 2019 Adri Liz Guti More. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    let appLogo: UIImageView = {
        let appLogo = UIImageView(frame: CGRect.zero)
//        appLogo.backgroundColor = UIColor.green
        appLogo.image = UIImage(named: "logo")
        appLogo.contentMode = UIView.ContentMode.scaleToFill
        appLogo.translatesAutoresizingMaskIntoConstraints = false
        return appLogo
    }()
    
    let userTextField: UITextField = {
        let userTextField = UITextField(frame: CGRect.zero)
        userTextField.placeholder = "Usuario"
        userTextField.clearButtonMode = UITextField.ViewMode.whileEditing
        userTextField.backgroundColor = UIColor.white
        userTextField.borderStyle = UITextField.BorderStyle.roundedRect
        userTextField.layer.borderWidth = 2.0
        userTextField.layer.borderColor = UIColor.systemBlue.cgColor
        userTextField.textColor = UIColor.black
        userTextField.keyboardType = UIKeyboardType.emailAddress
        userTextField.keyboardAppearance = UIKeyboardAppearance.dark
        userTextField.translatesAutoresizingMaskIntoConstraints = false
        return userTextField
    }()
    
    let passTextField: UITextField = {
        let passTextField = UITextField(frame: CGRect.zero)
        passTextField.placeholder = "Contraseña"
        passTextField.isSecureTextEntry = true
        passTextField.clearButtonMode = UITextField.ViewMode.whileEditing
        passTextField.backgroundColor = UIColor.white
        passTextField.borderStyle = UITextField.BorderStyle.roundedRect
        passTextField.layer.borderWidth = 2.0
        passTextField.layer.borderColor = UIColor.systemBlue.cgColor
        passTextField.textColor = UIColor.black
        passTextField.keyboardType = UIKeyboardType.default
        passTextField.keyboardAppearance = UIKeyboardAppearance.dark
        passTextField.translatesAutoresizingMaskIntoConstraints = false
        return passTextField
    }()
    
    let loginBtn: UIButton = {
        let loginBtn = UIButton(type: UIButton.ButtonType.system)
        loginBtn.frame = CGRect.zero
        loginBtn.backgroundColor = UIColor.systemBlue
        loginBtn.setTitle("Login", for: UIControl.State.normal)
        loginBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        loginBtn.layer.cornerRadius = 20.0
        loginBtn.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMaxYCorner]
        loginBtn.layer.masksToBounds = true
        loginBtn.translatesAutoresizingMaskIntoConstraints = false
        return loginBtn
    }()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    // Método para cargar los componentes de la vista
    private func setupView() {
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes de la vista
    private func setSubviews() {
        
        self.addSubview(appLogo)
        self.addSubview(userTextField)
        self.addSubview(passTextField)
        self.addSubview(loginBtn)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            appLogo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 50),
            appLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            appLogo.widthAnchor.constraint(equalToConstant: 150),
            appLogo.heightAnchor.constraint(equalToConstant: 150)
        ])
        
        NSLayoutConstraint.activate([
            userTextField.topAnchor.constraint(equalTo: appLogo.bottomAnchor, constant: 30),
            userTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            userTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25),
            userTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25)
        ])
        
        NSLayoutConstraint.activate([
            passTextField.topAnchor.constraint(equalTo: userTextField.bottomAnchor, constant: 30),
            passTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            passTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25),
            passTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25)
        ])
        
        NSLayoutConstraint.activate([
            loginBtn.topAnchor.constraint(equalTo: passTextField.bottomAnchor, constant: 100),
            loginBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25),
            loginBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25),
            loginBtn.heightAnchor.constraint(equalToConstant: 50)
        ])
        
//        NSLayoutConstraint.activate([
//            appLogo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 50),
//            appLogo.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -50),
//            appLogo.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -50),
//            appLogo.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 50)
//        ])
        
//        NSLayoutConstraint.activate([
//            appLogo.widthAnchor.constraint(equalToConstant: 150),
//            appLogo.heightAnchor.constraint(equalToConstant: 150),
//            appLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
//            appLogo.centerYAnchor.constraint(equalTo: self.centerYAnchor)
//        ])
        
//        NSLayoutConstraint.activate([
//            appLogo.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
//            appLogo.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1),
//            appLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
//            appLogo.centerYAnchor.constraint(equalTo: self.centerYAnchor)
//        ])
    }
    
}
